import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Example4 extends StatelessWidget {

  Example4({Key? key}) : super(key: key);

  final titles = [
    '....',
    'พรี้ลาม',
    'Pto',
    'Smile',
    'Arhhh',
    'Cat Ahhh',
    'Cat Beluga',
    'sad cat',
    'Doge'
  ];

  final images = [
    AssetImage('asset/img/img1.jpg'),
    AssetImage('asset/img/img2.jpg'),
    AssetImage('asset/img/img3.jpg'),
    AssetImage('asset/img/img4.jpg'),
    AssetImage('asset/img/img5.jpg'),
    AssetImage('asset/img/img6.jpg'),
    AssetImage('asset/img/img7.jpg'),
    AssetImage('asset/img/img8.jpg'),
    AssetImage('asset/img/img9.jpg')
  ];

  final subtitle = [
    'MEME1',
    'MEME2',
    'MEME3',
    'MEME4',
    'MEME5',
    'MEME6',
    'MEME7',
    'MEME8',
    'MEME9'
  ];
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView4'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              ListTile(
                leading: CircleAvatar(
                  backgroundImage: images[index],
                  radius: 30,
                ),
                 title: Text(
                  '${titles[index]}',
                  style: TextStyle(fontSize: 18),
                ),
                subtitle: Text(
                  subtitle[index],
                  style: TextStyle(fontSize: 15),
                ),
                trailing: const Icon(
                  Icons.add_box_outlined,
                  size: 25,
                ),
                onTap: (){
                      Fluttertoast.showToast(msg:'${titles[index]} \n ${subtitle[index]}',
                      toastLength: Toast.LENGTH_SHORT,
                      backgroundColor: Colors.orange,
                  );
                },
                tileColor:Colors.redAccent,
              ),
              const Divider(
                thickness: 10,
                height: 10,
                color: Colors. black12,

              ),
            ],
          );
        },
      ),
    );
  }
}
